# Markdown Cheat Sheet

Det här dokumentet är skrivet i ett format som kallas *Markdown*. Markdown är ett enkelt sätt att formattera textdokument.

## Rubriker

För att göra en rubrik skriver man en hash `#` i början av raden, som i början av rubriken i detta avsnitt.

Ju fler `#` i början av raden, desto mindre rubrik.

Skriv en `#` i början av raden där det står *Rubrik 1*, `##` på raden
*Rubrik 3* och `###` på raden *Rubrik 3*.

# Guuurl! You didn't!

## You didn't!

###### Oh Guurl!

## Betoning

För att få viss ord att sticka ut kan man betona det genom att skriva en
asterisk `*` framför och efter ordet.

Betona ordet *emphasis* i denna mening genom att omge det med `*`.

Betona orden **strong emphasis** i denna mening genom att omge dem båda med `**`.

## Listor

Det finns två typer av listor:

* Punktlistor
* Numrerade listor

Punktlistor börjar med en asterisk på varje rad. Man kan göra en fruktsallad av
ingredienserna nedan. Gör om dem till en punktlista.

* Banan
* Vindruvor
* Kiwi
* Passionsfrukt
* Mango

Numrerade listor börjar med nummer och punkt. Listan nedan är inte komplett.
Lägg till punkterna 3 - 5 så att det blir rätt.

1. Skala bananen och skär den i skivor.
2. Skölj vindruvorna och dela dem.
Skala kiwi och skär i tärningar.
Gröp ur passionsfrukten.
Skala mangon och skär i stavar.
6. Blanda alla frukter i en skål.

## Kod

Genom att börja en rad med fyra blanksteg eller en tab gör man att texten blir
ett kodblock.

Skriv 4 blanksteg i början av nedanstående rader för att göra ett kodblock.

    x = 3
    y = 5
    if x > y:
        print("x is bigger than y")
    else:
        print("y is bigger than x")
    x = foo() + bar

Kodavsnitt kan även göras genom att ange text inom ` tecken.

Denna rad innehåller lite `kod som renderas annorlunda`. Gör ett kodavsnitt av
resten av raden: x = foo() + bar

## Länkar

Du kan göra en länk till en annan sida genom att ange texten som ska vara en
länk inom hakparanteser, [ och ]. Efter texten inom hakparanteser ska det finnas
en address till sidan du ska länka till inom vanliga paranteser ( och ).

`En länk till [Markdown Syntax](http://daringfireball.net/projects/markdown/syntax)`

blir

En länk till [Markdown Syntax](http://daringfireball.net/projects/markdown/syntax)

Gör ordet Wikipedia i denna mening till en länk till Wikipedias hemsida.

## Bilder

En länk till en bild gör du så här:

`![Patwic](http://patwic.com/wp-content/uploads/2013/03/elephants_pyramid_1_transparent_web1.png)`

![Patwic](http://patwic.com/wp-content/uploads/2013/03/elephants_pyramid_1_transparent_web1.png)

![Söta Jonna](http://cdn1.cdnme.se/cdn/8-2/13230/images/2009/groda_62959109.jpg)

Texten inom hakparanteserna är en bildtext och bildens address anges inom
paranteser.

Gör en länk till en valfri bild på en elefant istället för texten på denna rad.
